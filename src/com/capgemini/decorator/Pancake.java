package com.capgemini.decorator;

public abstract class Pancake {

    public abstract String getDescription();
    public abstract double getCost();
}
